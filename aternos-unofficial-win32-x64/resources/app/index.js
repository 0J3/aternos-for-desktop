const { app, BrowserWindow, BrowserView } = require('electron')
const path = require('path')
// app.allowRendererProcessReuse=true

function createWindow () {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 1000,
    height: 500,
    backgroundColor: '#2e2c29',
    webPreferences: {
      nodeIntegration: false,
      enableRemoteModule: false,
      preload: path.join(__dirname, 'preload.js')
    }
  })

  // and load the index.html of the app.
  win.loadFile('index.html')

  
  
  // let view = new BrowserView()
  // // win.setBrowserView(view)
  // view.setBounds({ x: 0, y: 0, width: 1000, height: 420 })
  // view.setAutoResize({
  //   width: true,
  //   height: true,
  //   horizontal: true,
  //   vertical: true
  // })
  // view.webContents.loadFile('copyright.html')

  // let viewb = new BrowserView()
  // win.setBrowserView(viewb)
  // view=viewb
  // view.setBounds({ x: 0, y: 0, width: 1000, height: 500 })
  // view.setAutoResize({
  //   width: true,
  //   height: true,
  //   horizontal: true,
  //   vertical: true
  // })
  // view.webContents.loadURL('https://aternos.org')

  // setTimeout(() => {
  //   win.loadURL('https://aternos.org')
  // }, 500);

  // Open the DevTools.
  // win.webContents.openDevTools()

  // setTimeout(()=>{
    // win.setBrowserView(viewb)
  // },1000)

  // win.on('close', function(){
  //   win=null
  // })
}


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

module.exports = ""
